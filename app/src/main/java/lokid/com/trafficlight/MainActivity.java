package lokid.com.trafficlight;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * @author Alex Popova (14ИТ18к)
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button buttonRed;
    Button buttonYellow;
    Button buttonGreen;

    private int color = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonRed = (Button) findViewById(R.id.button_red);
        buttonYellow = (Button) findViewById(R.id.button_yellow);
        buttonGreen  = (Button) findViewById(R.id.button_green);


        buttonRed.setOnClickListener(this);
        buttonYellow.setOnClickListener(this);
        buttonGreen.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_red:
                setBackgroundColorView(R.color.colorRed);
                break;
            case R.id.button_yellow:
                setBackgroundColorView(R.color.colorYellow);
                break;
            case R.id.button_green:
                setBackgroundColorView(R.color.colorGreen);
                break;
        }
    }


    /**
     * @param color
     */
    public void setBackgroundColorView(int color){
        this.color = color;
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.activity_main);
        linearLayout.setBackgroundColor(ContextCompat.getColor(this, color));
    }



    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            color = savedInstanceState.getInt("color");
            setBackgroundColorView(color);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("color", color);
    }
}
